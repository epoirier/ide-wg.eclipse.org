local deployment = import "../../releng/hugo-websites/kube-deployment.jsonnet";

deployment.newProductionDeploymentWithStaging(
  "ide-wg.eclipse.org", "ide-wg-staging.eclipse.org"
)