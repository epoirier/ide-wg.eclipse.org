---
title: "Eclipse IDE Working Group"
seo_title: "Eclipse IDE Working Group"
headline: "Eclipse IDE Working Group"
tagline: "A new community-driven initiative to advance and sustain the Eclipse IDE suite of products and ecosystem"
date: 2021-05-04T10:00:00-04:00
layout: "single"
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
show_featured_story: true
#container: "container-fluid"
links: [[href: "#members", text: "Get Involved"]]
---

{{< members >}}

{{< sponsors >}}

{{< news >}}

{{< about >}}

{{< highlights >}}