/*!
 * Copyright (c) 2019 Eclipse Foundation, Inc.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * Contributors:
 *   Eric Poirier <eric.poirier@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/

(function($, document) {

    $('.matchheight-item').matchHeight();

    $.ajax({
        url: 'https://api.eclipse.org/public/member?pagesize=100&page=1&working_group=eclipse-ide',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $(data).each(function() {
                $('.ide-members-slider').append('<div class="featured-members-item item padding-20">' +
                    '<a target="_blank" href="'+ $(this)[0]['website'] +'">' + 
                        '<img alt="'+ $(this)[0]['name'] +'" class="img-responsive" src="'+ $(this)[0]['logos']['full'] +'">' +
                    '</a>' +
                '</div>');
            }); 

            $(".ide-members-slider").owlCarousel({
                responsive: {
                0: {
                    items: $(".ide-members-slider").data('slider-xs-count') || 1
                },
                768: {
                    items: $(".ide-members-slider").data('slider-sm-count') || 2
                },
                992: {
                    items: $(".ide-members-slider").data('slider-md-count') || 3
                },
                1170: {
                    items: $(".ide-members-slider").data('slider-lg-count') || 3
                }
                },
                pagination: true,
                responsiveRefreshRate: 100
            });
        },
        error: function () {
            console.log('Could not load members. MEMBER-01');
        },
    });
    
 })(jQuery, document); 